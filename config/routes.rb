Rails.application.routes.draw do
  namespace :api do
    # V1
    api_version(:module => "V1", :parameter => {:name => "version", :value => "v1"}) do
      resources :users, except: [:new, :edit]
    end

    #V2
    api_version(:module => "V2", :parameter => {:name => "version", :value => "v2"}, default: true) do
      resources :users, except: [:new, :edit]
    end
  end
end
