require 'rails_helper'


RSpec.describe Api::V2::UsersController, type: :controller do
  describe 'GET #index' do
    it 'returns a successful 200 response' do
      get :index, format: :json
      expect(response).to have_http_status(:success)
    end

    it 'returns all the users' do
      users = FactoryGirl.create_list(:user, 2)
      get :index, format: :json
      expect(response.body).to eq(users.to_json)
    end
  end

  describe 'GET #show' do
    let(:user) { FactoryGirl.create(:user) }

    it 'returns a successful 200 response' do
      get :show, id: user.id, format: :json
      expect(response).to have_http_status(:success)
    end

    it 'returns data of an single user' do
      get :show, id: user.id, format: :json
      expect(response.body).to eq(user.to_json)
    end

    it 'returns an error if the user doese not exist' do
      expect{
        get :show, id: 10, format: :json
      }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe 'POST #create' do
    context 'present name' do
      let(:valid_user_params) { FactoryGirl.attributes_for(:user) }

      it 'creates a new user' do
        expect{
          post :create, {user: valid_user_params}
        }.to change(User, :count).by(1)
      end

      it 'returns data of new user' do
        post :create, {user: valid_user_params}
        parsed_response = JSON.parse(response.body)
        expect(parsed_response).to_not be_nil
      end

      it 'returns a created status code' do
        post :create, {user: valid_user_params}
        expect(response).to have_http_status(:created)
      end
    end

    context 'empty name' do
      let(:invalid_user_params) { FactoryGirl.attributes_for(:user, name: nil) }

      it 'does not create a new user ' do
        expect{
          post :create, {user: invalid_user_params}
        }.to change(User, :count).by(0)
      end

      it 'returns a unprocessable_entity status code' do
        post :create, {user: invalid_user_params}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'PATCH #update' do
    let(:user) { FactoryGirl.create(:user) }

    context 'present name' do
      let(:valid_new_attributes) { FactoryGirl.attributes_for(:user) }

      it 'change user attributes' do
        patch :update, id: user.id, user: valid_new_attributes
        user.reload
        expect(user.name).to eq(valid_new_attributes[:name])
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'empty name' do
      let(:invalid_new_attributes) { FactoryGirl.attributes_for(:user, name: nil) }

      it 'returns a unprocessable_entity status code' do
        patch :create, id: user.id, user: invalid_new_attributes
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      @user = FactoryGirl.create(:user)
    end

    it 'deletes the user' do
      expect{
        delete :destroy, id: @user.id
      }.to change(User, :count).by(-1)
      expect(response).to have_http_status(:no_content)
    end
  end
end
